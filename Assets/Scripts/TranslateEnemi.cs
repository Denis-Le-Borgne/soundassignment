﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TranslateEnemi : MonoBehaviour {
	private float direction = 10;

	void Update () {
		if (transform.position.x >= 7.5) {
			direction = -10;
		} else if (transform.position.x <= -7.5) {
			direction = 10;
		}
		transform.Translate (new Vector3 (direction, 0, 0) * Time.deltaTime);
	}
}
