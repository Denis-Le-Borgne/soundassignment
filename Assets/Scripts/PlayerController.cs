﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;



public class PlayerController : MonoBehaviour
{
	public float speed;
	public Text countText;
	public Text winText;

	private Rigidbody rb;
	private int count;
	private bool onGround;
	private bool doubleJump;

	private AudioSource audio;

	void Start ()
	{
		rb = GetComponent<Rigidbody> ();
		audio = GetComponent<AudioSource>();
		onGround = true;
		doubleJump = true;
		count = 0;
		setCountText ();
		winText.text = "";
		rb.AddForce (new Vector3(0, 0, 50) * speed);
		Time.timeScale = 1;
	}

	void OnCollisionEnter(Collision collider)
	{
		if (collider.gameObject.CompareTag ("Ennemy") || transform.position.y <= 0) {
			winText.text = "You Lose!";
			setCountText ();
			Time.timeScale = 0;
		}
		if (collider.gameObject.CompareTag ("Win")) 
		{
			Time.timeScale = 0;
			winText.text = "You Win!";
			setCountText ();
		}

		if (collider.gameObject.tag == "Ground")
		{
			onGround = true;
			doubleJump = true;
		}
	}

	void OnCollisionExit(Collision collider)
	{
		if (collider.gameObject.tag == "Ground")
		{
			onGround = false;
		}
	}

	void Update()
	{
		if (Input.GetKeyDown (KeyCode.R)) {
			SceneManager.LoadScene (SceneManager.GetActiveScene().name);
		}
	}

	void FixedUpdate()
	{
		float maxSpeed = 30f;
		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");
		float moveJump = 0;
		if (Input.GetKeyDown (KeyCode.Space) && onGround && doubleJump) {
			moveJump = 25.0f;
		}/* else if (Input.GetKeyDown (KeyCode.Space) && doubleJump && !onGround) {
			moveJump = 15.0f;
			doubleJump = false;
		}*/
		Vector3 movement = new Vector3 (moveHorizontal, moveJump, moveVertical);
		rb.AddForce (movement * speed);
		if(rb.velocity.magnitude > maxSpeed)
		{
			rb.velocity = rb.velocity.normalized * maxSpeed;
		}

	}

	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.CompareTag("Pick Up"))
		{
			audio.Play();
			other.gameObject.SetActive (false);
			++count;
			setCountText ();
		}
	}

	void setCountText()
	{
		countText.text = "Count : " + count.ToString ();
	}
}
