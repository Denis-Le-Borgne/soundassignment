﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateEnemi : MonoBehaviour {
	public float direction;
	void Update () {
		transform.Rotate (new Vector3 (0, 0, direction) * Time.deltaTime);
	}
}
